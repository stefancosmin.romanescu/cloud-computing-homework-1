import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { WikipediaApiService } from "./services/wikipedia-api/wikipedia-api.service";
import { HttpClientJsonpModule, HttpClientModule } from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatButtonModule, MatCardModule, MatDividerModule, MatInputModule, MatListModule} from "@angular/material";
import {LanguageDetectApiService} from "./services/language-detect-api/language-detect-api.service";
import {FormsModule} from '@angular/forms';
import {TextRazorApiService} from "./services/text-razor-api/text-razor-api.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatDividerModule
  ],
  providers: [
    WikipediaApiService,
    LanguageDetectApiService,
    TextRazorApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
