import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class WikipediaApiService {

  private getBaseApiUrl(languageShortcut: string, topic: string) {
    return `https://${languageShortcut}.wikipedia.org/w/api.php?action=opensearch&search=${topic}&limit=4&namespace=0&format=json&callback=JSONP_CALLBACK`;
  }

  constructor(private http: HttpClient) {
  }

  public getResultFor(language: string, topic: string) {
    const url = this.getBaseApiUrl(language, topic);
    console.log(`GET ${url}`);
    // console.time('Wikipedia Time');
    const startTime = performance.now();
    return this.http.jsonp(this.getBaseApiUrl(language, topic), 'resultData')
      .do(_ => console.log(`Wikipedia Time : ${performance.now() - startTime}`))
      .do(data => console.log('Wikipedia response: ', data));
  }

  resultData(data): void {
    console.log(data);
  }
}
