import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";

@Injectable()
export class TextRazorApiService {

  private apiKey = 'addfb0163f85b3fe5af7095f62b927bb145456c57836a3b06f631708';
  private baseUrl = 'https://api.textrazor.com/';


  constructor(private http: HttpClient) { }

  analyze(text: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-TextRazor-Key': this.apiKey
      })
    };

    let urlParams = new HttpParams()
      .append('text', text)
      .append('extractors[]', 'entities')
      .append('extractors[]', 'topics');
    console.log('POST ' + this.baseUrl);
    console.time("TextRazor time");
    return this.http.post(this.baseUrl, urlParams.toString(), httpOptions)
      .do(res => {
        // console.log('TextRazor response arrived');
        console.timeEnd('TextRazor time');
      })
      .do(data => console.log('TextRazor response: ', data))
  }

}
