import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import "rxjs/add/operator/do";

@Injectable()
export class LanguageDetectApiService {
  private apiKey = '9838ee5e17bae3b0e5a8ffa4b59b2425';
  private apiUrl = 'http://ws.detectlanguage.com/0.2/detect';

  constructor(private http: HttpClient) { }

  detect(language: string) {
    let urlParams = new HttpParams();
    urlParams = urlParams.append('q', language);
    urlParams = urlParams.append('key', this.apiKey);

    let url = `${this.apiUrl}?${urlParams.toString()}`;

    console.log(`POST ${url}`);
    console.time('Language detect time');
    return this.http.post(url, '')
      .do(res => {
        // console.log('Language detect api response arrived');
        console.timeEnd('Language detect time');
      })
      .do(data => console.log('Language detect response: ', data))
  }

}
