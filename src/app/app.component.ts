import {Component} from '@angular/core';
import {WikipediaApiService} from "./services/wikipedia-api/wikipedia-api.service";
import {LanguageDetectApiService} from "./services/language-detect-api/language-detect-api.service";
import {TextRazorApiService} from "./services/text-razor-api/text-razor-api.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/zip';
import {ErrorObservable} from "rxjs/observable/ErrorObservable";
import {HttpErrorResponse} from "@angular/common/http";
import {catchError} from "rxjs/operators";

@Component({
  selector: 'tema1-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public searchResult = [];
  public userText: string = '';
  public errorMessage: string = '';

  constructor(private wikipediaApi: WikipediaApiService,
              private languageDetectApi: LanguageDetectApiService,
              private textRazorApi: TextRazorApiService) {

  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'The text language is not supported');
  };


  public search() {
    this.searchResult = [];
    this.errorMessage = '';

    let textRazorResult = this.textRazorApi.analyze(this.userText);
    let languageDetectResult = this.languageDetectApi.detect(this.userText);
    let detectedLanguage;
    let entities = [];
    let topics = [];

    Observable.zip(textRazorResult, languageDetectResult)
      .pipe(
        catchError(this.handleError)
      )
      .subscribe(data => {
          detectedLanguage = data[1].data.detections[0].language;
          // console.log(data[1]);
          if (data[0] && data[0].response) {
            let res = data[0].response;
            if (res.entities) {
              entities = res.entities.filter(entity => entity.confidenceScore > 1);
            }
            if (res.topics) {
              topics = res.topics.filter(topic => topic.score > 0.8);
            }
          }
          entities.forEach(entity => this.getWikipediaData(detectedLanguage, entity.entityId));
          topics.forEach(topic => this.getWikipediaData(detectedLanguage, topic.label));
        },
        error => this.errorMessage = error);
  }

  public getWikipediaData(language, topic) {
    this.wikipediaApi.getResultFor(language, topic)
      .subscribe((data: Array<any>) => {
        for (let i = 0; i < data[1].length; ++i) {
          if (!this.searchResult.find(element => element.link === data[3][i])) {
            this.searchResult.push({
              name: data[1][i],
              description: data[2][i],
              link: data[3][i]
            });
          }
        }
      });
  }
}
